// Zachary Mantle 22/10/17 Transaction Processing Coding Challenge
package CodingChallenge;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.text.*;

public class TRANSACTION_PROCESSING
{
	private static final int[] scheduledHours = {6, 21}; // Refers to the hours of the day the task will perform (24 hr format), you may add hours to this array
	private static final String absolutePath = System.getenv("$TRANSACTION_PROCESSING"); // Finds the $TRANSACTION_PROCESSING path as specified in requirements
	
	private static int totalAccounts = 0; // Indicates the total number of accounts in the report
	private static double credits = 0.00; // Indicates the credits section the report
	private static double debits = 0.00; // Indicates the debits section of the report
	private static int skippedTransactions = 0; // Indicates the number of skipped transaction
	
	/* Handles the timing part of the program
	 * Checks the time every 5 minutes and initiates the processing of the file if the time shown is correct */
	public static void main(String args[])
	{
		while(true) // The program will continue to run until closed
		{
		    try
		    {
			    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Australia/NSW"));
			    int hour = cal.get(Calendar.HOUR_OF_DAY);
			    for(int i = 0; i < scheduledHours.length; i++) // This allows further hours to be added to the array
			    {
			    	while (hour + 1 == scheduledHours[i])
				    {
				    	Thread.sleep(1000 * 60 * 5); // The program will pause every 5 minutes to load balance
				    	hour = cal.get(Calendar.HOUR_OF_DAY);
				    }
			    	while (scheduledHours[i] == hour)
			    	{
			    		ProcessFiles();
			    		Thread.sleep(1000 * 60 * 3);
			    		hour = cal.get(Calendar.HOUR_OF_DAY);
			    	}			    	
			    }
			    Thread.sleep(1000 * 60 * 60);
		    }
		    catch(Exception e)
		    {
		    	e.printStackTrace();
		    }
		}
	}
	
	/* Every viable file in the pending folder is gathered ready for processing */
	private static void ProcessFiles()
	{
		File[] pendingFiles = FindFiles(); // Gather an array of files ready to be processed
		try
		{
			// Next few lines perform the processing on the files
			for(File file : pendingFiles)
			{
				ReadFile(file); // Reads the contents of the file, populates variables ready for reporting
				if(CreateReport(file.getName())) // Creates the report in the desired format in the desired folder, returns true if occurs
					MoveFile(file.getName()); // Moves the source file to the process folder
				Reset(); // Resets the variables for the next file process
				Thread.sleep(2500); // Leave 2.5 seconds between each processed file to ensure FileNameExists error does not occur
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/* Method to only pick files in the format described in the requirements, returns the 
	 * array of files to the ProcessFiles() Method */
	private static File[] FindFiles()
	{
		File directory = new File(absolutePath + "/pending/");
		File[] pendingFiles = directory.listFiles(new FilenameFilter() 
		{
		    public boolean accept(File directory, String name) 
		    {
		        return(name.startsWith("finance_customer_transactions-$") && name.endsWith(".csv")); // The file must be a csv file with the prefix described in the requirements 
		    }
		});
		return(pendingFiles);
	}
	
	/* Creates the report as set out by the requirements, returns whether the report was made or not */
	private static boolean CreateReport(String fileName)
	{
		boolean reportSuccess = false;
		Date now = new Date();
		SimpleDateFormat reportFormat = new SimpleDateFormat("yyyyMMddhhmmss"); // Date format is changed to the required format
		String dateString = reportFormat.format(now);
		String reportFileName = absolutePath + "/reports/finance_customer_transactions_report-$" + dateString + ".csv"; // This is the name that the report will eventually be
		NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(); // Sets the formats that will be used by currency doubles
		NumberFormat integerFormat = NumberFormat.getNumberInstance(Locale.US); // Sets the formats that will be used by the number of items integers
		try
		{
			File outputReport = new File(reportFileName);
			if(outputReport.createNewFile())
			{
				PrintWriter pw = new PrintWriter(outputReport);
		        StringBuilder sb = new StringBuilder();
		        // Following lines creates the report in the correct format
		        sb.append("File Processed: " + fileName + "\r\n");
		        sb.append("Total Accounts: " + integerFormat.format(totalAccounts) + "\r\n");
		        sb.append("Total Credits : " + currencyFormat.format(credits) + "\r\n");
		        sb.append("Total Debits  : " + currencyFormat.format(debits) + "\r\n");
		        sb.append("Skipped Transactions: " + integerFormat.format(skippedTransactions));
		        pw.write(sb.toString());
		        pw.close();
				System.out.println("Report has been successfully created");
				reportSuccess = true;
			}
			else
			{
				System.out.println("Report name already exists");
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return(reportSuccess);
	}
	
	/* Reads the file and sets the global variables to what is required for the report */
	private static void ReadFile(File currentFile)
	{
        String line = "";
        String cvsSplitBy = ",";
        ArrayList<Integer> accountList = new ArrayList<Integer>(); // This array list will contain all the account numbers used in the account
        try (BufferedReader br = new BufferedReader(new FileReader(currentFile))) 
        {
        	br.readLine(); // Skips the first line
            while ((line = br.readLine()) != null) 
            {
                String[] inputs = line.split(cvsSplitBy);
                for(int i=0; i < inputs.length; i += 2) // Each account/transaction pair takes up 2 spots of the array
                {
                	try
                	{
                		int customerAccount = Integer.parseInt(inputs[i]);
                		if(!accountList.contains(customerAccount)) // If the account list already contains the account number, the total accounts is not incremented
                		{
                			totalAccounts++;
                			accountList.add(customerAccount); // Ensures the following account numbers are checked
                		}
                		double transactionAmount = Double.parseDouble(inputs[i + 1]);
                		if(transactionAmount >= 0) // Adds amount to credits or debits as required
                		{
                			credits += transactionAmount;            			
                		}
                		else
                		{
                			debits -= transactionAmount;
                		}
                	}
                	catch(Exception e)
                	{
                		skippedTransactions++; // If the customer account could not be parsed the transaction is skipped and the amounts are not processed
                	}
                }
            }
        } 
        catch (IOException e) 
        {
            e.printStackTrace();
        }       
	}
	
	/* Moves the source file to the correct folder, indicating the file has been processed */
	private static void MoveFile(String fileName)
    {
		String fromPath = absolutePath + "/pending/";
		String toPath = absolutePath + "/processed/";
	    try
	    {
	    	Files.move(Paths.get(fromPath + fileName), Paths.get(toPath + fileName));
	    }
	    catch(IOException e)
	    {
	    	System.out.println("Unable to move file");
	    }
    }
	
	/* Resets all global variables to the original setting, ready for the next file */
	private static void Reset()
	{
		totalAccounts = 0;
		credits = 0.00;
		debits = 0.00;
		skippedTransactions = 0;
	}
}
