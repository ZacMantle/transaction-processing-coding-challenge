Transaction Processing, Zachary Mantle 22/10/17
As part of The Greater Bank's Pre-interview coding challenge

Please find in this directory my coding challenge response developed in Eclipse Oxygen

In this directory you will find

src-> CodingChallenge -> TRANSACTION_PROCESSING 
	This contains all code written to run the program, 
	note: this program will work with a setup $TRANSACTION_PROCESSING environment variable
	note: if you wish to test the program outside the required hours (6, 21), simply change the values at the top of this file, recompile and run

reports -> 
	Contains sample output reports created by my program

processed -> 
	Contains source files moved to processed folder by the program

pending ->
	Contains source files that have yet to be processed